package com.example.listecourse

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity

object Tools {
    fun showDialogSubscribe(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Notification")
        builder.setMessage("Pour pouvoir utiliser cette fonctionnalité vous devez vous abonner")

        builder.setNegativeButton("Fermer") { t, _ ->
            t.dismiss()
        }

        builder.setPositiveButton("S'abonner") { _, _ ->
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://ec2-50-16-133-35.compute-1.amazonaws.com"))
            context.startActivity(browserIntent)
        }

        builder.show()
    }
}