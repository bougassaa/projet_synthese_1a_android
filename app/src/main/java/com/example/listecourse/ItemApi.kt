package com.example.listecourse

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

class ItemApi(): Api() {

    fun getItems(listId: Int): List<Items>? {
        val inputStream = request("", "item/list/$listId", "GET")

        val mapper = jacksonObjectMapper()

        if(inputStream != null) {
            return mapper.readValue(inputStream)
        }

        return null
    }

    fun update(item: Items) {
        request(item.toString(), "item", "PUT", true)
    }

    fun add(item: Items) {
        request(item.toString(), "item", "POST", true)
    }

    fun remove(itemId: String) {
        request("", "item/$itemId", "DELETE")
    }
}
