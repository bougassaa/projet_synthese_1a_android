package com.example.listecourse

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import kotlinx.android.synthetic.main.activity_main3.*
import kotlinx.android.synthetic.main.sharelist_dialog.view.*
import org.threeten.bp.LocalDateTime

class MainActivity3 : AppCompatActivity() {
    private lateinit var listview: ListView
    private lateinit var currentList: ListItems
    private lateinit var context: Activity
    private lateinit var itemEdit: Items

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val list = intent.getSerializableExtra("list") as ListItems

        listview = list_items_listview
        currentList = list
        context = this

        actionBar?.title = list.shop
        supportActionBar?.title = list.shop

        val dateTime = LocalDateTime.parse(list.date?.dropLast(1))

        listTitle.text = list.shop
        listDate.text = "Crée le : " + dateTime.toLocalDate().toString()

        DisplayItems().execute()

        btAdd.setOnClickListener {
            if(etItemName.text.isEmpty() || etItemQuantity.text.isEmpty()) {
                Toast.makeText(this@MainActivity3, "Ne pas laisser les champs vides.", Toast.LENGTH_LONG).show()
            } else {
                val item = Items(
                    label = etItemName.text.toString(),
                    quantity = etItemQuantity.text.toString().toInt(),
                    checked = 0,
                    list = list.id)

                AddItem(item).execute()
            }
        }

        btDoEdition.setOnClickListener {
            if(etItemName.text.isEmpty() || etItemQuantity.text.isEmpty()) {
                Toast.makeText(this@MainActivity3, "Ne pas laisser les champs vides.", Toast.LENGTH_LONG).show()
            } else {
                itemEdit.quantity = etItemQuantity.text.toString().toInt()
                itemEdit.label = etItemName.text.toString()
                UpdateItem(itemEdit).execute()

                it.visibility = View.GONE
                btAdd.visibility = View.VISIBLE
                etItemName.editableText.clear()
                etItemQuantity.setText("1")
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.list_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.archivList -> {
                currentList.archived = 1
                UpdateList().execute()
                true
            }
            R.id.deleteList -> {
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Confirmation")
                builder.setMessage("Voulez vous vraiment supprimé cette liste ?")

                builder.setNegativeButton("Annuler") { t, _ ->
                    t.dismiss()
                }

                builder.setPositiveButton("Oui") { _, _ ->
                    RemoveList().execute()
                }

                builder.show()

                true
            }
            R.id.shareList -> {
                CanShare().execute()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showShareDialog() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.sharelist_dialog, null)

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setTitle("Partager la liste")

        val  mAlertDialog = mBuilder.show()

        mDialogView.btCloseDialog.setOnClickListener {
            mAlertDialog.dismiss()
        }

        mDialogView.etUsername.doAfterTextChanged {
            val words = it.toString()

            if(words.isNotEmpty())
                UsersByType(words, mDialogView, mAlertDialog).execute()
        }
    }

    inner class ItemAdapter(val datas: List<Items>): BaseAdapter() {

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("ViewHolder")
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val inflater = context.layoutInflater
            val rowView = inflater.inflate(R.layout.custom_listitem, null, true)

            val item = datas[position]

            val lblQuantity = rowView.findViewById<TextView>(R.id.itemQuantity)
            val lblTitle = rowView.findViewById<TextView>(R.id.itemName)
            val btRemoveItem = rowView.findViewById<Button>(R.id.btRemove)
            val btCheckItem = rowView.findViewById<Button>(R.id.btCheckItem)
            val btEditItem = rowView.findViewById<Button>(R.id.btEdit)

            lblQuantity.text = item.quantity.toString()
            lblTitle.text = item.label

            if(item.checked == 1) {
                btCheckItem.visibility = View.GONE
                btEditItem.visibility = View.GONE
                rowView.setBackgroundColor(resources.getColor(R.color.checked))
            } else {
                btCheckItem.setOnClickListener { v: View? ->
                    item.checked = 1
                    UpdateItem(item).execute()
                }

                btEditItem.setOnClickListener {
                    itemEdit = item

                    context.findViewById<Button>(R.id.btAdd).visibility = View.GONE
                    context.findViewById<Button>(R.id.btDoEdition).visibility = View.VISIBLE

                    val etItemQuantity = context.findViewById<EditText>(R.id.etItemQuantity)
                    etItemQuantity.setText(item.quantity.toString())

                    val etItemName = context.findViewById<EditText>(R.id.etItemName)

                    etItemName.setText(item.label)
                    etItemName.requestFocus()

                    val imm: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.showSoftInput(etItemName, InputMethodManager.SHOW_IMPLICIT)
                }
            }

            btRemoveItem.setOnClickListener {
                RemoveItem(item.id.toString()).execute()
            }

            return rowView
        }

        override fun getItem(position: Int): Any {
            return position
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return datas.count()
        }
    }

    inner class DisplayItems: AsyncTask<Void, Void, List<Items>>() {

        override fun doInBackground(vararg params: Void?): List<Items>? {
            val api = ItemApi()
            return api.getItems(currentList.id!!)
        }

        override fun onPostExecute(result: List<Items>) {
            listview.adapter = ItemAdapter(result)

            if(result.isEmpty()) {
                context.findViewById<TextView>(R.id.empty_list).visibility = View.VISIBLE
            }
        }
    }

    inner class UpdateItem(val item: Items): AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            val api = ItemApi()
            api.update(item)
            return null
        }

        override fun onPostExecute(result: Void?) {
            DisplayItems().execute()
        }
    }

    inner class AddItem(val item: Items): AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            val api = ItemApi()
            api.add(item)
            return null
        }

        override fun onPostExecute(result: Void?) {
            DisplayItems().execute()

            context.findViewById<EditText>(R.id.etItemName).text.clear()
            context.findViewById<EditText>(R.id.etItemQuantity).setText("1")
        }
    }

    inner class RemoveItem(val itemId: String): AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            val api = ItemApi()
            api.remove(itemId)
            return null
        }

        override fun onPostExecute(result: Void?) {
            DisplayItems().execute()
        }
    }

    inner class UpdateList: AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            val api = ListApi()
            api.archivList(currentList)
            return null
        }

        override fun onPostExecute(result: Void?) {
            val intent = Intent(context, MainActivity2::class.java)
            startActivity(intent)
        }
    }

    inner class RemoveList: AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            val api = ListApi()
            api.remove(currentList.id!!)
            return null
        }

        override fun onPostExecute(result: Void?) {
            val intent = Intent(context, MainActivity2::class.java)
            startActivity(intent)
        }
    }

    inner class UsersByType(
        val words: String,
        val dialog: View,
        val mBuilder: AlertDialog
    ): AsyncTask<Void, Void, List<MutableMap<*, *>>?>() {

        override fun doInBackground(vararg params: Void?): List<MutableMap<*, *>>? {
            return UserApi().userByType(words)
        }

        override fun onPostExecute(result: List<MutableMap<*, *>>?) {
            val username = mutableListOf<String>()
            if (result != null) {
                if(result.isNotEmpty()) {
                    dialog.emptyUserList.visibility = View.GONE

                    for (user in result) {
                        username.add(user["name"] as String)
                    }

                    dialog.usersListview.adapter = ArrayAdapter(this@MainActivity3, android.R.layout.simple_list_item_1, username)

                    dialog.usersListview.setOnItemClickListener { _, _, position, _ ->
                        dialog.usersListview.visibility = View.GONE
                        dialog.etUsername.visibility = View.GONE

                        dialog.shareForm.visibility = View.VISIBLE
                        dialog.btSaveShare.visibility = View.VISIBLE
                        dialog.lblChoosenUser.text = result[position]["name"].toString()

                        dialog.btSaveShare.setOnClickListener {
                            ShareList(result[position]["id"] as Int, dialog.cbShare.isChecked).execute()
                            mBuilder.dismiss()
                        }
                    }

                    return
                }
            }

            dialog.emptyUserList.visibility = View.VISIBLE
            dialog.usersListview.adapter = ArrayAdapter(this@MainActivity3, android.R.layout.simple_list_item_1, username)
        }
    }

    inner class CanShare: AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void?): Boolean {
            return UserApi().checkIfSubscriber()
        }

        override fun onPostExecute(result: Boolean?) {
            if (result!!) {
                showShareDialog()
            } else {
                Tools.showDialogSubscribe(this@MainActivity3)
            }
        }
    }

    inner class ShareList(
        val user: Int,
        val edit: Boolean
    ): AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            ListApi().share(currentList.id!!, user, edit)
            return null
        }
    }
}