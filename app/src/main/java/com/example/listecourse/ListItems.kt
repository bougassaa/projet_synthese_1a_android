package com.example.listecourse

import java.io.Serializable

class ListItems(
    val id: Int? = null,
    val shop: String? = null,
    val date: String? = null,
    var archived: Int? = null,
    val user: Int? = null) : Serializable {

    override fun toString(): String {
        return shop as String
    }

    fun getJson(): String {
        return """{"id" : $id, "shop" : "$shop", "date" : "$date", "archived" : $archived, "user" : $user}"""
    }
}