package com.example.listecourse

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.InputStream
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

open class Api() {

    private val apiUrl = "http://50.16.133.35:3333/"

    companion object {
        var token: String? = null
    }

    protected fun request(params: String, url: String, method: String = "POST", json: Boolean = false): InputStream? {
        var sUrl = "$apiUrl$url"

        if(method == "GET") {
            sUrl += "?$params"
        }

        val mUrl = URL(sUrl)

        with(mUrl.openConnection() as HttpURLConnection) {
            requestMethod = method

            if(token != null) {
                setRequestProperty("Authorization", "Bearer $token")
            }

            if (json) {
                setRequestProperty("Content-Type", "application/json");
            }

            if(method != "GET") {
                val wr = OutputStreamWriter(outputStream)
                wr.write(params)
                wr.flush()
            }

            if(responseCode != 200) {
                return null
            }

            return inputStream
        }
    }

    protected fun mapToQuery(map: Map<String, String>): String {
        var query = "";

        for ((key, value) in map) {
            query += "$key=$value&"
        }

        return query
    }

    fun complexResult(params: String, url: String, method: String = "POST", json: Boolean = false) : List<MutableMap<*, *>>? {
        val inputStream = request(params, url, method)

        val mapper = jacksonObjectMapper()
        if(inputStream != null) {
            return mapper.readValue(inputStream)
        }
        return null
    }

    fun simpleResult(params: String = "", url: String, method: String = "POST", json: Boolean = false) : MutableMap<*, *>? {
        val inputStream = request(params, url, method)

        val mapper = jacksonObjectMapper()
        if(inputStream != null) {
            return mapper.readValue(inputStream)
        }
        return null
    }
}