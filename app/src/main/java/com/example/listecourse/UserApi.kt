package com.example.listecourse

class UserApi: Api() {
    fun authenticate(email: String, password: String): Boolean {
        val p = mapOf("email" to email, "password" to password)

        val rep = simpleResult(mapToQuery(p), "user/authenticate")

        if(rep != null) {
            token = rep["token"] as String?
            return true
        }

        return false
    }

    fun userByType(words: String): List<MutableMap<*, *>>? {
        return complexResult("typing=$words", "user/typing", json = true)
    }

    fun checkIfSubscriber(): Boolean {
        val rep = simpleResult(url = "user/checkIfSubscriber", method = "GET")

        if(rep != null) {
            return rep["subscriber"] as Int? == 1
        }

        return false
    }
}