package com.example.listecourse

data class Items(
    val id: Int? = null,
    var label: String? = null,
    var quantity: Int? = null,
    var checked: Int? = null,
    val list: Int? = null
) {
    override fun toString(): String {
        return """{"id" : $id, "label" : "$label", "quantity" : $quantity, "checked" : $checked, "list" : $list}"""
    }
}