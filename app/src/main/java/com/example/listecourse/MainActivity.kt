package com.example.listecourse

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnCnx = findViewById<Button>(R.id.buttonCnx)

        actionBar?.title = "Bienvenue"
        supportActionBar?.title = "Bienvenue"

        btnCnx.setOnClickListener {
            val etEmail = findViewById<EditText>(R.id.editTextTextEmailAddress);
            val etPassword = findViewById<EditText>(R.id.editTextTextPassword);

            if(etEmail.text.isEmpty() || etPassword.text.isEmpty()) {
                Toast.makeText(this@MainActivity, "Ne pas laisser les champs vides.", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            Authenticate(etEmail.text.toString(), etPassword.text.toString()).execute()
        }
    }

    inner class Authenticate(private val email: String, private val password: String): AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void?): Boolean {
            val api = UserApi()
            return api.authenticate(email, password)
        }

        override fun onPostExecute(result: Boolean?) {
            if (result == true) {
                val intent = Intent(this@MainActivity, MainActivity2::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this@MainActivity, "Adresse e-mail ou mot de passe erroné, ou bien votre compte a été bloqué ou non validé par e-mail.", Toast.LENGTH_LONG).show()
            }
        }
    }
}