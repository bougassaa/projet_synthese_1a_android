package com.example.listecourse

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

class NotificationApi: Api() {

    fun count(): Int {
        val res = complexResult("", "notification/unread", "GET")

        return res?.count() ?: 0
    }

    fun getAll(): List<Notification>? {
        val inputStream = request("", "notification", "GET")

        val mapper = jacksonObjectMapper()

        if(inputStream != null) {
            return mapper.readValue(inputStream)
        }

        return null
    }

    fun check(notifId: String) {
        request("", "notification/$notifId", "PUT")
    }
}