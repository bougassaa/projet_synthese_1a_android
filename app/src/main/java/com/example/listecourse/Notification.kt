package com.example.listecourse

data class Notification(
    val id: Int? = null,
    var title: String? = null,
    var message: String? = null,
    var user: Int? = null,
    val seen: Boolean? = null
)