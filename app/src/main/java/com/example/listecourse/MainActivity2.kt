package com.example.listecourse

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.listcreation_dialog.view.*
import kotlinx.android.synthetic.main.notification_dialog.view.*
import kotlinx.android.synthetic.main.sharelist_dialog.view.btCloseDialog


class MainActivity2 : AppCompatActivity() {
    private lateinit var listView: ListView
    private lateinit var context: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        actionBar?.title = "Listes ouvertes"
        supportActionBar?.title = "Listes ouvertes"

        context = this

        listView = findViewById<ListView>(R.id.recipe_list_view)
        DisplayList().execute()

        btCreateList.setOnClickListener {
            CanCreateList().execute()
        }
    }

    override fun onBackPressed() {
        // remove return button here
    }

    private lateinit var notificationCount: RelativeLayout
    private lateinit var lvNotif: ListView

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        val item1: MenuItem = menu.findItem(R.id.actionbar_item)
        MenuItemCompat.setActionView(item1, R.layout.notification_update_count_layout)

        notificationCount = MenuItemCompat.getActionView(item1) as RelativeLayout

        notificationCount.findViewById<Button>(R.id.btNotif).setOnClickListener {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.notification_dialog, null)

            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Notifications")

            val  mAlertDialog = mBuilder.show()

            mDialogView.btCloseDialog.setOnClickListener {
                mAlertDialog.dismiss()
            }

            lvNotif = mDialogView.lvNotification
            DisplayNotification().execute()
        }

        CountNotification(notificationCount).execute()

        return super.onCreateOptionsMenu(menu)
    }

    inner class DisplayList(): AsyncTask<Void, Void, List<ListItems>>() {

        override fun doInBackground(vararg params: Void?): List<ListItems>? {
            return ListApi().getCurrentsLists()
        }

        override fun onPostExecute(result: List<ListItems>) {
            val adapter = ArrayAdapter(this@MainActivity2, android.R.layout.simple_list_item_1, result)
            listView.adapter = adapter

            listView.setOnItemClickListener { _, _, position, _ ->
                val intent = Intent(this@MainActivity2, MainActivity3::class.java)
                intent.putExtra("list", result[position])
                startActivity(intent)
            }
        }
    }

    inner class CountNotification(val notificationCount: RelativeLayout) : AsyncTask<Void, Void, Int>() {

        override fun doInBackground(vararg params: Void?): Int {
            return NotificationApi().count()
        }

        override fun onPostExecute(result: Int?) {
            val etNotifCount = notificationCount.findViewById<TextView>(R.id.etNotifCount)

            if (result != 0) {
                etNotifCount.visibility = View.VISIBLE
                etNotifCount.text = result.toString()
            } else {
                etNotifCount.visibility = View.GONE
            }
        }
    }

    inner class DisplayNotification : AsyncTask<Void, Void, List<Notification>>() {

        override fun doInBackground(vararg params: Void?): List<Notification>? {
            return NotificationApi().getAll()
        }

        override fun onPostExecute(result: List<Notification>) {
            lvNotif.adapter = NotificationAdapter(result)
        }
    }

    inner class CheckNotif(val notifId: String) : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            NotificationApi().check(notifId)
            return null
        }

        override fun onPostExecute(result: Void?) {
            DisplayNotification().execute()
            CountNotification(notificationCount).execute()
        }
    }

    inner class NotificationAdapter(val datas: List<Notification>): BaseAdapter() {

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @SuppressLint("ViewHolder")
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val inflater = context.layoutInflater
            val rowView = inflater.inflate(R.layout.notification_listitem, null, true)

            val notification = datas[position]

            val lblNotifTitle = rowView.findViewById<TextView>(R.id.lblNotifTitle)
            val lblNotifMessage = rowView.findViewById<TextView>(R.id.lblNotifMessage)
            val btCheckNotif = rowView.findViewById<Button>(R.id.btCheckNotif)

            lblNotifTitle.text = notification.title
            lblNotifMessage.text = notification.message

            if (!notification.seen!!) {
                rowView.setBackgroundColor(resources.getColor(R.color.overlay))

                btCheckNotif.visibility = View.VISIBLE

                btCheckNotif.setOnClickListener {
                    CheckNotif(notification.id.toString()).execute()
                }
            }

            return rowView
        }

        override fun getItem(position: Int): Any {
            return position
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return datas.count()
        }
    }

    inner class CanCreateList: AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void?): Boolean {
            return UserApi().checkIfSubscriber()
        }

        override fun onPostExecute(result: Boolean?) {
            if (result!!) {
                showCreateListDialog()
            } else {
                Tools.showDialogSubscribe(this@MainActivity2)
            }
        }
    }

    inner class CreateList(
        val list: ListItems,
        val mAlertDialog: AlertDialog
    ): AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void?): Void? {
            ListApi().insert(list)
            return null
        }

        override fun onPostExecute(result: Void?) {
           DisplayList().execute()
            mAlertDialog.dismiss()
        }
    }

    private fun showCreateListDialog() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.listcreation_dialog, null)

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setTitle("Nouvelle liste")

        val  mAlertDialog = mBuilder.show()

        mDialogView.btCloseDialog.setOnClickListener {
            mAlertDialog.dismiss()
        }

        mDialogView.btSaveList.setOnClickListener {
            val name = mDialogView.etListName.text.toString()

            if(name.isEmpty()) {
                Toast.makeText(this@MainActivity2, "Ne pas laisser le champ vide.", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val list = ListItems(shop = name)
            CreateList(list, mAlertDialog).execute()
        }
    }
}