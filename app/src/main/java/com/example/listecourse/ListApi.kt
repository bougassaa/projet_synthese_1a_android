package com.example.listecourse

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

class ListApi(): Api() {
    fun getCurrentsLists(): List<ListItems>? {
        val inputStream = request("", "list", "GET")

        val mapper = jacksonObjectMapper()

        if(inputStream != null) {
            return mapper.readValue(inputStream)
        }

        return null
    }

    fun archivList(list: ListItems) {
        request(list.getJson(), "list", "PUT", true)
    }

    fun remove(listId: Int) {
        request("", "list/$listId", "DELETE")
    }

    fun share(list: Int, user: Int, edit: Boolean) {
        request("""{"list":$list,"user":$user,"edit":$edit}""", "share", json = true)
    }

    fun insert(list: ListItems) {
        request(list.getJson(), "list",json = true)
    }
}